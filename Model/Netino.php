<?php

namespace La\NetinoModerationBundle\Model;

class Netino
{

    const UNDEFINED_MODERATION_CAUSE = 99;
    const MODERATOR_ID = 'netino';

    public static function getModerationCauseById($id)
    {
        $moderation_causes = array(
            2 => "Agression",
            13 => "Appel au suicide",
            18 => "Attaque contre religion",
            12 => "Blague interdite",
            19 => "Copyright non respecté",
            1 => "Diffamation",
            5 => "Illégal",
            14 => "Illisible",
            3 => "Insulte",
            16 => "Manque de respect envers victime",
            10 => "Pédophilie",
            6 => "Pseudo insultant",
            9 => "Pub",
            17 => "Prosélytisme religieux",
            15 => "Prostitution",
            4 => "Racisme",
            11 => "Sexe, nu, érotisme, porno",
            8 => "SPAM",
            7 => "Vie privée",
            static::UNDEFINED_MODERATION_CAUSE => "Autre motif",
        );

        if (in_array($id, $moderation_causes)) {
            return $moderation_causes[$id];
        }
        return $moderation_causes[static::UNDEFINED_MODERATION_CAUSE];
    }


}