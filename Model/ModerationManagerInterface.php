<?php

namespace La\NetinoModerationBundle\Model;


interface ModerationManagerInterface
{
    const ACK_IDS_SEPARATOR = ";";
    const RESULT_STATUS_OK = "OK";
    const RESULT_STATUS_NOT_OK = "NOK";
    const RESULT_OK = 'OK';
    const RESULT_ERROR = "ERROR";
    const RESULT_NOT_FOUND = 'NOTFOUND';

    /**
     * Process /new request from Netino
     */
    public function processNew();

    /**
     * Process /ack request from Netino
     *
     * @param $idString : content_ids concatenated with static::ACK_IDS_SEPARATOR
     */
    public function processAck($idString);

    /**
     * Process /result request from Netino
     */
    public function processResult($id, $status, $moderationCause);

}