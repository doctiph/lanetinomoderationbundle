<?php

namespace La\NetinoModerationBundle\Model;

use La\CommentBundle\Model\ModerationManagerInterface;

class ThreadsManager extends ModerationManager
{
    const NEW_COMMENTS_SENT_COUNT = 100;

    /**
     * @var \La\CommentBundle\Model\ModerationManagerInterface
     */
    protected $moderationManager;

    /**
     * @param ModerationManagerInterface $moderationManager
     */
    public function __construct(ModerationManagerInterface $moderationManager)
    {
        $this->moderationManager = $moderationManager;
    }

    /**
     * @return array
     */
    public function processNew()
    {
        return $this->moderationManager->findLastPendingComments(0, static::NEW_COMMENTS_SENT_COUNT);
    }

    /**
     * @param $idString
     * @internal param array $ids
     * @return array
     */
    public function processAck($idString)
    {
        $ids = explode(static::ACK_IDS_SEPARATOR, $idString);
        $ack = array();

        foreach ($ids as $id) {

            $comment = $this->moderationManager->getValidPendingCommentById($id);
            if (is_null($comment)) {
                $ack[] = array('id' => $id, 'result' => static::RESULT_NOT_FOUND);
                continue;
            }

            if ($this->moderationManager->setInProgress($comment) !== false) {
                $ack[] = array('id' => $id, 'result' => static::RESULT_OK);
            } else {
                $ack[] = array('id' => $id, 'result' => static::RESULT_ERROR);
            }
        }

        return $ack;
    }

    /**
     * @param $id
     * @param $status
     * @param $moderationCause
     * @return array
     */
    public function processResult($id, $status, $moderationCause)
    {
        $comment = $this->moderationManager->getValidInProgressCommentById($id);

        if(is_null($comment))
        {
            return array('result' => static::RESULT_NOT_FOUND, 'error' => 'Invalid ID');
        }

        if ($status == static::RESULT_STATUS_OK) {
            $state = ModerationManagerInterface::MODERATION_OK;
        } elseif ($status == static::RESULT_STATUS_NOT_OK) {
            $state = ModerationManagerInterface::MODERATION_NOT_OK;
        } else {
            return array('result' => static::RESULT_ERROR, 'error' => 'Invalid status');
        }

        if ($this->moderationManager->setModerated($comment, $state, $moderationCause, Netino::MODERATOR_ID) !== false) {
            return array('result' => static::RESULT_OK);
        } else {
            return array('result' => static::RESULT_ERROR, 'error' => 'Could not update comment.');
        }

    }

    /**
     * @param $commentId
     * @param $bannedUntil
     * @param $banMessage
     * @return bool
     */
    public function processBan($commentId, $bannedUntil, $banMessage)
    {

        return $this->moderationManager->ban($commentId, $bannedUntil, $banMessage, Netino::MODERATOR_ID);
    }

}