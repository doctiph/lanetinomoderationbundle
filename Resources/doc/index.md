NetinoModerationBundle
============

# Prerequis

Ce bundle requiert LaCommentBundle.

# Installation

## update composer.json :

    "repositories": [
	(...)
        {
            "type": "git",
            "url": "git@git-pool01.in.ladtech.fr:LaNetinoModerationBundle"
        }
    ],
    "require": {
    	(...)
        "la/netinomoderationbundle": "dev-master"
    },

## app/config/routing.yml

    la_comment_netino_moderation:
        resource: "@LaNetinoModerationBundle/Resources/config/routing.yml"
        prefix: /

## app/config/security.yml

Rajouter le rôle de modérateur API

    role_hierarchy:
        (...)
        ROLE_API_MODERATOR: ROLE_API_CLIENT

    (...)

    access_control:
            (...)
            - { path: ^/api/threads/moderate, roles: ROLE_API_MODERATOR }
            - { path: ^/api$, roles: ROLE_API_CLIENT }


## Update kernel

    # /app/AppKernel.php

     public function registerBundles()
         {
             $bundles = array(
     	    (...)
                 new La\NetinoModerationBundle\LaNetinoModerationBundle()
     	    );
     	    (...)
         }


# Voir aussi

La documentation du système de modération Netino se trouve dans confluence :

http://jira.lagardere-active.com/confluence/download/attachments/394634/2%20Sp%C3%A9cifications%20techniques%20liaison%20informatique%20Moderatus%20Webservice%20XML%20v2.7.pdf?api=v2
