<?php

namespace La\NetinoModerationBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ThreadsController extends Controller
{

    /**
     * Return pending comments
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'xml');

        try {

            return $this->render(
                'LaNetinoModerationBundle:Comments:new.xml.twig',
                array('messages' => $this->get('la_comment.netino_moderation.threads.manager')->processNew()),
                $response
            );

        } catch (\Exception $e) {

            return $this->render(
                'LaNetinoModerationBundle:Comments:new.xml.twig',
                array(
                    'messages' => array(),
                    'error' => $e->getMessage()
                ),
                $response
            );

        }
    }

    /**
     * Flag processing comments in database, and return acknowledgement
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function ackAction(Request $request)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'xml');


        if (!is_null($request->get('content_id'))) {

            return $this->render(
                'LaNetinoModerationBundle:Comments:ack.xml.twig',
                array('ack' => $this->get('la_comment.netino_moderation.threads.manager')->processAck($request->get('content_id'))),
                $response
            );

        } else {

            return $this->render(
                'LaNetinoModerationBundle:Comments:ack.xml.twig',
                array('ack' => array(array('id' => 0, 'result' => 'ERROR', 'comment' => 'No content id found in request.'))),
                $response
            );

        }
    }

    /**
     * flag moderated comments (validated / censored) and return result
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resultAction(Request $request)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'xml');

        $manager = $this->get('la_comment.netino_moderation.threads.manager');

        $result = $manager->processResult(
            $request->get('content_id'), // Id commentaire
            $request->get('status'), // Status moderation
            $request->get('motif_id') // Motif modération
        );

        $manager->processBan(
            $request->get('content_id'), // Id commentaire
            $request->get('banned_until'), // Si il y a un ban, date de fin de ban (si pas de ban, == null)
            $request->get('message') // Si il y a un ban, message du ban (si pas de ban, == null)
        );

        return $this->render(
            'LaNetinoModerationBundle:Comments:result.xml.twig',
            array(
                "result" => $result['result'],
                "error" => isset($result['error']) ? $result['error'] : null
            ),
            $response
        );
    }

}
