<?php

namespace La\NetinoModerationBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ThreadsControllerTest extends WebTestCase
{


    public function testNew()
    {
        $client = static::createClient(array(), array('HTTP_HOST' => 'profile.develop'));
        $entityManager = $client->getContainer()->get('doctrine')->getManager();


        $method = 'GET';
        $uri = 'api/threads/moderate/netino/new';
        $credentials = array(
            'PHP_AUTH_USER' => 'netino',
            'PHP_AUTH_PW' => '13e93e80be7579dc46bc3dd81e77fa0e2f73ee7f',
        );

        $this->assertTrue($this->failedAuthenticationTest($method, $uri));

        $crawler = $client->request($method, $uri, array(), array(), $credentials);

        $this->assertTrue($client->getResponse()->getCharset() == 'UTF-8');
        $this->assertTrue(substr_count($client->getResponse()->getContent(), '<?xml version="1.0" encoding="utf-8"?>') == 1);

        $this->assertTrue(substr_count($crawler->html(), '<contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '</contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), "<nb_content>") == 1);
        $this->assertTrue(substr_count($crawler->html(), "</nb_content>") == 1);
    }

    public function testAck()
    {
        $method = 'GET';
        $credentials = array(
            'PHP_AUTH_USER' => 'netino',
            'PHP_AUTH_PW' => '13e93e80be7579dc46bc3dd81e77fa0e2f73ee7f',
        );

        $this->assertTrue($this->failedAuthenticationTest($method, 'api/threads/moderate/netino/ack?content_id=1;2;3;4'));

        // Failing 1 = no content id passed in uri
        $client = static::createClient(array(), array('HTTP_HOST' => 'profile.develop'));
        $crawler = $client->request($method, 'api/threads/moderate/netino/ack', array(), array(), $credentials);

        $this->assertTrue($client->getResponse()->getCharset() == 'UTF-8');
        $this->assertTrue(substr_count($client->getResponse()->getContent(), '<?xml version="1.0" encoding="utf-8"?>') == 1);

        $this->assertTrue(substr_count($crawler->html(), '<contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '</contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), 'result="ERROR"') == 1);
        $this->assertTrue(substr_count($crawler->html(), 'content_id="0"') == 1);

        // Failing 2= wrong content id passed in uri
        $client = static::createClient(array(), array('HTTP_HOST' => 'profile.develop'));
        $crawler = $client->request($method, 'api/threads/moderate/netino/ack?content_id=paul;virginie', array(), array(), $credentials);

        $this->assertTrue($client->getResponse()->getCharset() == 'UTF-8');
        $this->assertTrue(substr_count($client->getResponse()->getContent(), '<?xml version="1.0" encoding="utf-8"?>') == 1);

        $this->assertTrue(substr_count($crawler->html(), '<contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '</contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), 'content_id') == 2);
        $this->assertTrue(substr_count($crawler->html(), 'result="NOTFOUND"') == 2);

        // Success
        $client = static::createClient(array(), array('HTTP_HOST' => 'profile.develop'));
        $crawler = $client->request($method, 'api/threads/moderate/netino/ack?content_id=1;2;3;4', array(), array(), $credentials);

        $this->assertTrue($client->getResponse()->getCharset() == 'UTF-8');
        $this->assertTrue(substr_count($client->getResponse()->getContent(), '<?xml version="1.0" encoding="utf-8"?>') == 1);

        $this->assertTrue(substr_count($crawler->html(), '<contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '</contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), 'content_id') == 4);
        $this->assertTrue(substr_count($crawler->html(), 'result') == 4);
    }

    public function testResult()
    {
        $method = 'GET';
        $credentials = array(
            'PHP_AUTH_USER' => 'netino',
            'PHP_AUTH_PW' => '13e93e80be7579dc46bc3dd81e77fa0e2f73ee7f',
        );

        $this->assertTrue($this->failedAuthenticationTest($method, '/api/threads/moderate/netino/result?content_id=142&status=NOK&motif_id=12&banned_until=2014-01-10 12:00:00&message=pas%20de%20bol'));

        // Failing 1 = no content id passed in uri
        $client = static::createClient(array(), array('HTTP_HOST' => 'profile.develop'));
        $crawler = $client->request($method, '/api/threads/moderate/netino/result', array(), array(), $credentials);

        $this->assertTrue($client->getResponse()->getCharset() == 'UTF-8');
        $this->assertTrue(substr_count($client->getResponse()->getContent(), '<?xml version="1.0" encoding="utf-8"?>') == 1);

        $this->assertTrue(substr_count($crawler->html(), '<contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '</contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '<result>NOTFOUND</result>') == 1);

        // Failing 2= wrong status passed in uri
        $client = static::createClient(array(), array('HTTP_HOST' => 'profile.develop'));
        $crawler = $client->request($method, '/api/threads/moderate/netino/result?content_id=michel&status=NOTOK', array(), array(), $credentials);

        $this->assertTrue($client->getResponse()->getCharset() == 'UTF-8');
        $this->assertTrue(substr_count($client->getResponse()->getContent(), '<?xml version="1.0" encoding="utf-8"?>') == 1);

        $this->assertTrue(substr_count($crawler->html(), '<contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '</contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '<result>NOTFOUND</result>') == 1);

        // Success OK
        $client = static::createClient(array(), array('HTTP_HOST' => 'profile.develop'));
        $crawler = $client->request($method, '/api/threads/moderate/netino/result?content_id=142&status=OK', array(), array(), $credentials);

        $this->assertTrue($client->getResponse()->getCharset() == 'UTF-8');
        $this->assertTrue(substr_count($client->getResponse()->getContent(), '<?xml version="1.0" encoding="utf-8"?>') == 1);

        $this->assertTrue(substr_count($crawler->html(), '<contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '</contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '<result>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '</result>') == 1);

        // Success not OK
        $client = static::createClient(array(), array('HTTP_HOST' => 'profile.develop'));
        $crawler = $client->request($method, '/api/threads/moderate/netino/result?content_id=142&status=NOK&motif_id=12&banned_until=2014-01-10 12:00:00&message=pas%20de%20bol', array(), array(), $credentials);

        $this->assertTrue($client->getResponse()->getCharset() == 'UTF-8');
        $this->assertTrue(substr_count($client->getResponse()->getContent(), '<?xml version="1.0" encoding="utf-8"?>') == 1);

        $this->assertTrue(substr_count($crawler->html(), '<contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '</contents>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '<result>') == 1);
        $this->assertTrue(substr_count($crawler->html(), '</result>') == 1);
    }



    /**
     * Return true if authentication was needed
     *
     * @param $method
     * @param $uri
     * @return bool
     */
    public function failedAuthenticationTest($method, $uri)
    {
        $client = static::createClient(array(), array('HTTP_HOST' => 'profile.develop'));
        $client->request($method, $uri);
        if($client->getResponse()->getStatusCode() != 200)
        {
            return true;
        }else{
            return false;
        }
    }
}
